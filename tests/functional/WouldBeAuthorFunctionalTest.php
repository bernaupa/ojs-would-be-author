<?php

/**
 * @file tests/functional/WouldBeAuthorFunctionalTest.php
 *
 *
 * @class WouldBeAuthorFunctionalTest
 * @ingroup plugins_generic_wouldBeAuthorPlugin
 *
 * @brief Functional tests for the wouldBeAuthor plugin.
 */

import('lib.pkp.tests.WebTestCase');

class WouldBeAuthorFunctionalTest extends WebTestCase {
	/** @var string A hand-picked reviewer-only username from test database */
	protected $reviewer = 'agallego';

	/**
	 * @copydoc WebTestCase::getAffectedTables
	 */
	protected function getAffectedTables() {
		return PKP_TEST_ENTIRE_DB;
	}

	/**
	 * @see WebTestCase::setUp()
	 */
	protected function setUp() {
		parent::setUp();

		$this->start();

		$this->enablePlugin();
	}

	/**
	 * Enable/disable the plugin
	 */
	private function enablePlugin($enable = TRUE) {
		$this->logIn('admin', 'admin');

		$this->waitForElementPresent($selector = 'link=Website');
		$this->clickAndWait($selector);
		$this->click('link=Plugins');

		// find the plugin
		$this->waitForElementPresent($selector = '//input[starts-with(@id, \'select-cell-wouldbeauthorplugin-enabled\')]');
		// enable/disable if necessary
		if ($enable && !$this->isChecked($selector)) {
			// do enable the plugin
			$this->click($selector);
			$this->waitForElementPresent('//div[contains(.,\'The plugin "Would-Be Author" has been enabled.\')]');
		} else if (!$enable && $this->isChecked($selector)) {
			// do disable the plugin
			$this->click($selector);

			// confimation popup
			$this->waitForElementPresent('css=div.pkp_modal_confirmation');
			$this->click('css=a.pkpModalConfirmButton');
                        $this->waitForElementNotPresent('css=div.pkp_modal_confirmation');

			$this->waitForElementPresent('//div[contains(.,\'The plugin "Would-Be Author" has been disabled.\')]');
		}

		$this->logOut();
	}

	/**
	 * Check the reviewer can not make a submission without the plugin
	 */
	function testWouldBeAuthor_noPlugin() {
		// disable the plugin
		$this->enablePlugin(FALSE);

		$this->logIn($this->reviewer);

		$this->waitForElementPresent('css=[id=myQueue]');
		$this->assertElementNotPresent('link=New Submission');
	}

	/**
	 * Check the plugin does not mess up with direct submission
	 */
	function testWouldBeAuthor_withoutUser() {
		$this->open(self::$baseUrl . '/index.php/publicknowledge/submission/wizard');

		$this->assertLocation(self::$baseUrl . '/index.php/publicknowledge/login\?*');
	}

	/**
	 * Check the reviewer can access the submission wizard
	 */
	function testWouldBeAuthor() {
		$this->logIn($this->reviewer);

		// check new submission button available
		$this->waitForElementPresent('css=[id=myQueue]');
		$this->assertElementPresent($selector = 'link=New Submission');
		$this->clickAndWait($selector);
		// successfully redirected to submission wizard
		$this->waitForElementPresent('//h1[contains(text(), \'Submit an Article\')]');
	}
}
