<?php

/**
 * @defgroup plugins_generic_wouldBeAuthor Would-be Author plugin
 */

/**
 * @file index.php
 *
 * @ingroup plugins_generic_wouldBeAuthor
 * @brief Wrapper for Would-be Author plugin.
 *
 */
require_once('WouldBeAuthorPlugin.inc.php');

return new WouldBeAuthorPlugin();
