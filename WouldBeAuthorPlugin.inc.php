<?php

/**
 * @file WouldBeAuthorPlugin.inc.php
 *
 * @class WouldBeAuthorPlugin
 * @ingroup plugins_generic_wouldBeAuthorPlugin
 *
 * @brief Plugin for allowing reviewer to make a submission.
 *
 * It enables the 'New Submission' button on the dashboard of a reviewer. When
 * pressed it automatically adds the user to the author group before proceeding
 * with the regular submission wizard.
 */

import('lib.pkp.classes.plugins.GenericPlugin');

class WouldBeAuthorPlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
				HookRegistry::register('TemplateManager::display',array($this, 'enableNewSubmissionButton'));

				HookRegistry::register('LoadHandler', array($this, 'transformIntoAuthor'));
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.wouldBeAuthor.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.wouldBeAuthor.displayName');
	}

	/**
	 * Display the 'New Submission' button on dashboard to reviewers.
	 *
	 * @param string $hookName
	 * @param array $args
	 * @return boolean
	 */
	function enableNewSubmissionButton($hookName, $args) {
		$request =& Registry::get('request');
		$templateManager =& $args[0];
		$template = $args[1];

		if ($template !== 'dashboard/index.tpl') return false;

		// monkey patching the SubmissionListPanel Vue.js computed:
		// add reviewer to users allowed to add submission
		$submissionsListPanelPatch = <<<EOD
if (typeof pkp.controllers.SubmissionsListPanel !== 'undefined') {
	originalComputed = pkp.controllers.SubmissionsListPanel.computed.currentUserCanAddSubmission;

	pkp.controllers.SubmissionsListPanel.computed.currentUserCanAddSubmission = function() {
		return originalComputed() || pkp.userHasRole(pkp.const.ROLE_ID_REVIEWER);
	};
}
EOD;
		$templateManager->addJavaScript(
				'wouldbeauthor',
				$submissionsListPanelPatch,
				array(
					'priority' => STYLE_SEQUENCE_LAST,
					'contexts' => 'backend',
					'inline' => true
				));

		return false;
	}

	/**
	 * Turn would-be author into authors by adding role.
	 *
	 * @param type $hookName
	 * @param type $args
	 * @return boolean
	 */
	function transformIntoAuthor($hookName, $args) {
		$page = $args[0];
		$op = $args[1];

		/* only necessary when entering the submission wizard */
		if (!($page === 'submission' && $op === 'wizard')) return false;

		$request = Registry::get('request');
		$contextId = $request->getContext()->getId();

		$user = $request->getUser();
		/* skip if no user */
		if (!$user) return false;
		/* check user is actually missing the author role */
		if ($user->hasRole(ROLE_ID_AUTHOR, $contextId)) return false;

		if ($user->hasRole(ROLE_ID_REVIEWER, $contextId)) {
			/* add user to the default author group */
			$userGroupDao = DAORegistry::getDAO('UserGroupDAO');
			$authorUserGroup = $userGroupDao->getDefaultByRoleId($contextId, ROLE_ID_AUTHOR);

			$userGroupDao->assignUserToGroup($user->getId(), $authorUserGroup->getId());
		}

		return false;
	}
}
