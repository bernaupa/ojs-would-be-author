# Would-be author plugin for OJS


## About

This [OJS](https://pkp.sfu.ca/ojs/) plugin simplifies the creation of submisions by reviewers.

It enables the 'New Submission' button on the dashboard of every reviewers and allows them to make a submission whether or not they had the author role.


## System requirements

This plugin has been tested on OJS version 3.1.0.0.


## Installation

To install:
 - unpack the plugin archive to OJS's plugins/generic directory;
 - enable the plugin by ticking the checkbox for "Would-Be Author" in the set of generic plugins available ('Management' > 'Website Settings' > 'Plugins' > 'Generic Plugins').

**Note**:
Make sure the name of the directory for the plugin is 'wouldBeAuthor', not 'ojs-would-be-author'.
